#!/usr/bin/env php
<?php
/**
 * OpenSQLManager
 *
 * Free Database manager for Open Source Databases
 *
 * @package		OpenSQLManager
 * @author 		Timothy J. Warren
 * @copyright	Copyright (c) 2012
 * @link 		https://github.com/aviat4ion/OpenSQLManager
 * @license 	http://philsturgeon.co.uk/code/dbad-license
 */

// --------------------------------------------------------------------------

/**
 * Bootstrap file
 *
 * Initializes parent window and starts the GTK event loop
 */

// --------------------------------------------------------------------------

// Suppress errors that php-gtk puts out
error_reporting(-1 & ~(E_STRICT));

// Set the stupid timezone so PHP shuts up.
date_default_timezone_set('GMT');

// Don't set an arbitrary memory limit!
ini_set('memory_limit', -1);

// Set the current directory as the base for included files
define('BASE_DIR', dirname(__FILE__).'/sys');
define('SETTINGS_DIR', dirname(__FILE__));
define('PROGRAM_NAME', 'OpenSQLManager');
define('VERSION', '0.1.0pre');

// --------------------------------------------------------------------------

/**
 * Log fatal errors
 */
function log_fatal()
{
	// Catch the last error
	$error = error_get_last();

	// types of errors that are fatal
	$fatal = array(E_ERROR, E_PARSE, E_RECOVERABLE_ERROR);

	// Log error.
	if(in_array($error['type'], $fatal))
	{
		file_put_contents('errors.txt', print_r($error, TRUE), FILE_APPEND);
	}
}

register_shutdown_function('log_fatal');

// --------------------------------------------------------------------------

// Make sure php-gtk works
if ( ! class_exists('gtk'))
{
	trigger_error("PHP-gtk not found. Please load the php-gtk2 extension in your php.ini", E_USER_ERROR);
	die();
}

// Make sure pdo exists
if( ! class_exists('pdo'))
{
	trigger_error("PHP support for PDO is required.", E_USER_ERROR);
	die();
}

// --------------------------------------------------------------------------

/**
 * Error handler to convert errors to exceptions
 *
 * @param int $errno
 * @param string $errstr
 * @param string $errfile
 * @param int $errline
 * @throws ErrorException
 */
function exception_error_handler($errno, $errstr, $errfile, $errline)
{
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}

// Do this after the two compatibility checks for cleaner output
// Note that this will throw exceptions on notices
set_error_handler("exception_error_handler", E_ERROR | E_WARNING | E_NOTICE);

// --------------------------------------------------------------------------

if ( ! function_exists('do_include'))
{
	/**
	 * Bulk directory loading workaround for use
	 * with array_map and glob
	 *
	 * @param string $path
	 * @return void
	 */
	function do_include($path)
	{
		require_once($path);
	}
}

// --------------------------------------------------------------------------

/**
 * Autoloader for OpenSQLManager
 *
 * @param string $class
 */
function osm_autoload($class)
{
	$class = strtolower($class);
	$widget_path = BASE_DIR . "/widgets/{$class}.php";
	$window_path = BASE_DIR . "/windows/{$class}.php";
	
	if (is_file($widget_path))
	{
		require_once($widget_path);
	}
	elseif (is_file($window_path))
	{
		require_once($window_path);
	}
}

// --------------------------------------------------------------------------

// Load everything so that we don't have to do requires later
require_once(BASE_DIR . '/common/functions.php');
spl_autoload_register('osm_autoload');

// --------------------------------------------------------------------------

// Auto-load db drivers
require_once(BASE_DIR . "/db/autoload.php");

// --------------------------------------------------------------------------

// Create the main window
new Main();

// Start the GTK event loop
GTK::main();

// End of index.php