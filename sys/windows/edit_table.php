<?php
/**
 * OpenSQLManager
 *
 * Free Database manager for Open Source Databases
 *
 * @package		OpenSQLManager
 * @author 		Timothy J. Warren
 * @copyright	Copyright (c) 2012
 * @link 		https://github.com/aviat4ion/OpenSQLManager
 * @license 	http://philsturgeon.co.uk/code/dbad-license
 */

// --------------------------------------------------------------------------

/**
 * Window for editing db table structure
 *
 * @package OpenSQLManager
 * @subpackage Windows
 */
class Edit_Table extends GtkWindow {

	/**
	 * Database fields for the current database table
	 *
	 * @var array
	 */
	private $fields;

	/**
	 * Create the window, and set basic properties
	 *
	 * @param string $name
	 * @param array $fields
	 */
	public function __construct($name="", array $fields=array())
	{
		// Create the window
		parent::__construct();

		if ( ! empty($name))
		{
			$this->set_title($name);
		}

		if ( ! empty($fields))
		{
			$this->fields = $fields;
		}
	}

}
// End of edit_table.php