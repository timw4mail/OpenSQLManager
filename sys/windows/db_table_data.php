<?php
/**
 * OpenSQLManager
 *
 * Free Database manager for Open Source Databases
 *
 * @package		OpenSQLManager
 * @author 		Timothy J. Warren
 * @copyright	Copyright (c) 2012
 * @link 		https://github.com/aviat4ion/OpenSQLManager
 * @license 	http://philsturgeon.co.uk/code/dbad-license
 */

// --------------------------------------------------------------------------

/**
 * Popup window to display database table data
 *
 * @package OpenSQLManager
 * @subpackage Windows
 */
class DB_Table_Data extends GtkWindow {

	/**
	 * Reference to the scrolled window
	 * 
	 * @var GtkScrolledWindow
	 */
	protected $win;

	/**
	 * Create and populate the window
	 *
	 * @param array $data
	 * @return void
	 */
	public function __construct($data)
	{
		parent::__construct();

		$this->set_title("Table Data");
		$this->set_position(Gtk::WIN_POS_CENTER_ALWAYS);
		$this->set_destroy_with_parent(TRUE);

		// Add the scrolled window
		$this->win = new GTKScrolledWindow();
		$this->win->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
		$this->add($this->win);

		// Resize to a sane size
		$this->set_size_request(640, 480);

		// Layout the widgets
		$view = new Data_Grid();
		$view->render_data($data);

		// Add the grid to the window
		$this->win->add_with_viewport($view);

		// Show everything
		$this->show_all();
	}
}
