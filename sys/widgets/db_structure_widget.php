<?php
/**
 * OpenSQLManager
 *
 * Free Database manager for Open Source Databases
 *
 * @package		OpenSQLManager
 * @author 		Timothy J. Warren
 * @copyright	Copyright (c) 2012
 * @link 		https://github.com/aviat4ion/OpenSQLManager
 * @license 	http://philsturgeon.co.uk/code/dbad-license
 */

// --------------------------------------------------------------------------

/**
 * Class for generating db-structure editing views
 *
 * @package OpenSQLManager
 * @subpackage Widgets
 */
class DB_Structure_Widget extends GTKTable {

	/**
	 * Initialize the class
	 */
	public function __construct()
	{
		parent::__construct();
	}

}
// End of db_structure_widget.php
