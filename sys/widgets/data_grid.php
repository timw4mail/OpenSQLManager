<?php
/**
 * OpenSQLManager
 *
 * Free Database manager for Open Source Databases
 *
 * @package		OpenSQLManager
 * @author 		Timothy J. Warren
 * @copyright	Copyright (c) 2012
 * @link 		https://github.com/aviat4ion/OpenSQLManager
 * @license 	http://philsturgeon.co.uk/code/dbad-license
 */

// --------------------------------------------------------------------------

/**
 * Class to simplify dealing with GtkTreeView
 *
 * @package OpenSQLManager
 * @subpackage Widgets
 */
class Data_Grid extends GtkTreeView {

	/**
	 * GtkTreeStore object
	 * 
	 * @var GtkTreeStore
	 */
	protected $model;

	/**
	 * Create the object
	 *
	 * @param object $model
	 */
	public function __construct($model = null)
	{
		if ( ! is_null($model))
		{
			$this->model = $model;
			parent::__construct($this->model);
		}
		else
		{
			parent::__construct();
		}
	}

	// --------------------------------------------------------------------------

	/**
	 * Get the value of the model for the current selection
	 *
	 * @param int pos
	 * @return mixed
	 */
	public function get($pos = 0)
	{
		if ( ! is_numeric($pos))
		{
			return parent::get($pos);
		}

		// Get the selection object of the row
		$sel = $this->get_selection();

		// Get the model and iterator for the selected row
		list($model, $iter) = $sel->get_selected();

		// Return on lack of $iter
		if (is_null($iter))
		{
			return;
		}

		// Get the data from the model
		return $model->get_value($iter, $pos);
	}

	// --------------------------------------------------------------------------

	/**
	 * Clear datagrid
	 *
	 * @return void
	 */
	public function reset()
	{
		$this->model->clear();

		$cols = $this->get_columns();

		foreach($cols as &$c)
		{
			$this->remove_column($c);
		}
	}

	// --------------------------------------------------------------------------

	/**
	 * Adds a column of data to the model
	 *
	 * @param GtkTreeViewColumn $col
	 * @param GtkCellRenderer $cell
	 * @param GtkTreeModel $model
	 * @param GtkTreeIter $iter
	 * @param int $i
	 * @return void
	 */
	public function add_data_col($col, $cell, $model, $iter, $i=0)
	{
		$data = $model->get_value($iter, $i);

		if (empty($data))
		{
			return;
		}

		$col->set_visible(TRUE);
		$cell->set_property('text', $data);
	}

	// --------------------------------------------------------------------------

	/**
	 * Create a datagrid from the provided data array
	 *
	 * @param array $data
	 * @param array $events
	 * @return void
	 */
	public function render_data($data, $events=array())
	{
		if ( ! is_array($data))
		{
			return;
		}

		$model = new StdClass();

		$cols =  ( ! empty($data)) ? array_keys($data[0]) : array();

		// Add columns to model
		$col_num = (count($cols) > 0) ? count($cols) : 1;
		$model_args = array_fill(0, $col_num, Gobject::TYPE_PHP_VALUE);
		$eval_string = '$model = new GTKTreeStore('.implode(',', $model_args).');';

		// Shame, shame, but how else?
		eval($eval_string);
		$this->set_model($model);
		$this->set_headers_clickable(TRUE);

		// Set the data in the model
		for($i=0, $c = count($data); $i < $c; $i++)
		{
			// Add a row
			$row = $model->insert($i);

			$j = -1;
			$vals = array($row);
			foreach($data[$i] as &$v)
			{
				$vals[] = ++$j;
				$vals[] = trim($v);
			}

			call_user_func_array(array($model, 'set'), $vals);
		}

		// Add columns to view
		foreach($cols as $i => &$c)
		{
			$renderer = new GtkCellRendererText();
			$renderer->set_property('editable', TRUE);
			$this->insert_column_with_data_func($i, $c, $renderer, array($this, 'add_data_col'), $i);
		}
	}

}
// End of data_grid.php