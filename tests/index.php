<?php
/**
 * OpenSQLManager
 *
 * Free Database manager for Open Source Databases
 *
 * @package		OpenSQLManager
 * @author 		Timothy J. Warren
 * @copyright	Copyright (c) 2012
 * @link 		https://github.com/aviat4ion/OpenSQLManager
 * @license 	http://philsturgeon.co.uk/code/dbad-license
 */

// --------------------------------------------------------------------------

/**
 * Unit test bootstrap - Using php simpletest
 */
define('OSL_TEST_DIR', dirname(__FILE__).'/');
define('OSL_BASE_DIR', str_replace(basename(OSL_TEST_DIR).'/', '', OSL_TEST_DIR).'sys/');

// --------------------------------------------------------------------------

// Include db autoloader
require_once(OSL_BASE_DIR . 'db/autoload.php');

// Include db tests
require_once(OSL_BASE_DIR . 'db/tests/index.php');


